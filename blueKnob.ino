#include <Rotary.h>
#include <Keyboard.h>

#include <Adafruit_NeoPixel.h>         // Include Adafruit NeoPixel library
#define PIN            10               // First LED on digital pin 6
#define NUMLEDS        12             // Use total of 2 LEDs

Adafruit_NeoPixel leds = Adafruit_NeoPixel(NUMLEDS, PIN, NEO_GRB + NEO_KHZ800);

// Variables will change:
int counter = NUMLEDS - 1;
const int WIN_PALETTE = 0;
const int APPLE_PALETTE = 1;
int currentPalette = WIN_PALETTE;
int RGB_PALETTES[2][3]={{39,232,232},{229,217,207}};

const int buttonPin = 2;     // the number of the pushbutton pin
Rotary r = Rotary(4, 3);

void setup() {
  leds.begin();                       // Initialize 'leds' object
  leds.setBrightness(64);
  leds.show();                        // Set new values
  attachInterrupt(digitalPinToInterrupt(buttonPin),  isr, RISING);
  Keyboard.begin();
}
void isr() {
  Keyboard.releaseAll();
}
void loop() {
  unsigned char result = r.process();
  if (result) {
    if (result == DIR_CCW) {
      Keyboard.release(KEY_LEFT_SHIFT);
      if (!--counter) {
        counter = NUMLEDS - 1;
      }
    } else {
      Keyboard.press(KEY_LEFT_SHIFT);
      ++counter;
    }
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.write(KEY_TAB);

    counter = abs(counter % NUMLEDS);

    for (int nled = NUMLEDS - 1; nled >= 0; nled--) {
      if (nled == counter) {
        leds.setPixelColor(nled, leds.Color(RGB_PALETTES[currentPalette][0], RGB_PALETTES[currentPalette][1], RGB_PALETTES[currentPalette][2]));  // Color mix for LED0
      } else {
        leds.setPixelColor(nled, leds.Color(0, 0, 0));  // Color mix for LED0}
      }
      leds.show();
      // Delay a little bit to avoid bouncing
      delay(5);
    }
  }
}
